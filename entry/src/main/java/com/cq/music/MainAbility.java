package com.cq.music;

import com.cq.music.util.Logger;
import ohos.ace.ability.AceAbility;
import ohos.aafwk.content.Intent;
import ohos.bluetooth.BluetoothHost;




public class MainAbility extends AceAbility {


    @Override
    public void onStart(Intent intent) {
        //    蓝牙部分_申请位置权限
        String[] permissions = {"ohos.permission.LOCATION"};
        // 判断是否具有该权限
        // 1,否  ,去申请
        //2,有,不申请,不做处理
//        if(){
//            requestPermissionsFromUser(permissions, 0);
//            super.onStart(intent);
//        }else{
//            super.onStart(intent);
//        }


        BlueToothAbility.register(this);
        requestPermissionsFromUser(permissions, 0);
            super.onStart(intent);
    }

    @Override
    public void onStop() {
        super.onStop();
        BlueToothAbility.unregister();
    }


    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        super.onAbilityResult(requestCode, resultCode, resultData);
        Logger.d("onAbilityResult requestCode = "
                + requestCode + " ;resultCode =" + resultCode);
    }
}
