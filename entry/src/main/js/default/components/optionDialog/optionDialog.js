
export default {
    props: {
        functionListTxt: {
            type: String,
            default: '',
        },

        iconOn: {
            type: String,
            default: "/common/power/ic_standing.png",
        },
        iconOff: {
            type: String,
            default: "",
        },
       // 功能展示下标
        showListIndex: {
            type: Number,
            default: null,
        },
        //
        seletValue: {
            type: Number,
            default: 0,
        },
        rangeTextList:{
            type: String,
            default: "",
        },
        rangeTimeList:{
            type: String,
            default: "",
        },
        hourUnit:{
            type: String,
            default: "",
        },
        unit:{
            type: String,
            default: "天",
        }
    },
    data: {
        isTopFiring: true,
        topFiring: "顶部加热",
        isRinse: false,
        rinse: '清洗',
        isFloodLight: false,
        floodLight: '照明灯',
        isSteamingRoastLights: false,
        steamingRoastLights: '蒸烤灯光',
        showDialog:false,
        textvalue:'default textvalue',
        defaultTarg:1,
        seletHourValue:'',
        seletHourValues:'',
        seletValues:'',
    },


    onInit() {
    },
    clickTopFiring () {
        this.isTopFiring = !this.isTopFiring
    },
    clickRinse () {
        this.isRinse = !this.isRinse
    },
    clickFloodLight () {
        this.isFloodLight = !this.isFloodLight
    },
    clickSteamingRoastLights () {
        this.isSteamingRoastLights = !this.isSteamingRoastLights
    },
    onOPen (val){
        console.info('-----------'+val)
        this.$element('simpledialog').show()
    },
    //弹框取消定时
    bnt_CancelClick(e){
        this.$element('simpledialog').close();
        console.log(this.showListIndex+111111+JSON.stringify(e))
        if(this.showListIndex==3 || this.showListIndex==4){
            if(e=='0'){
                this.seletValue = ' '
            }
        }
    },
    //弹框取消
    bnt_ConfirmClick(){
        this.seletHourValue=this.seletHourValues
        this.seletValue=this.seletValues
        this.$element('simpledialog').close();
    },
    //小时弹框滚动选中
    handleChangeHour_targ(val){
        this.seletHourValues = val.newValue+val.target.attr.indicatorsuffix;
    },
    //弹框滚动选中
    handleChange_targ(val) {
        console.log('弹框选中' + JSON.stringify(val))
        this.seletValues = val.newValue+val.target.attr.indicatorsuffix;
        console.log(JSON.stringify(val.newSelected))
    },
}