const TAG = "DragAcr"

export default class DragAcr {
    constructor(param) {
        this.initParam(param)
        this.draw(this.value)
    }

    initParam(param) {
        const {
            el,
            startAngle = -0.5,
            endAngle = 1.5,
            innerColor = "#51e6b6",
            outColor = "#08000000",
            innerLineWidth = 1,
            outLineWidth = 24,
            counterclockwise = false,
            slider = 8,
            color = ["#86c1ff", "#2B5BF9"],
            sliderColor = "#ffffff",
            sliderBorderColor = "#33aaff",
            value = 0,
            change = (v) => {
                console.log(v)
            },
            textShow = true,
        } = param;

        this.el = el;
        this.width = 252;
        this.height = 252;
        this.center = this.width / 2
        this.outLineWidth = outLineWidth;
        this.radius = this.width / 2 - this.outLineWidth / 2;
        // this.ctx = el.getContext("2d");
        this.ctx = el.getContext('2d', {
            antialias: true
        });

        this.startAngle = startAngle;
        this.endAngle = endAngle;
        this.innerColor = innerColor;
        this.outColor = outColor;
        this.innerLineWidth = innerLineWidth;
        this.counterclockwise = counterclockwise;
        this.slider = slider;
        this.color = color;
        this.sliderColor = sliderColor;
        this.sliderBorderColor = sliderBorderColor;
        this.value = value;
        this.textShow = textShow;
        this.change = change;
        this.isDown = false;

        console.log(TAG + ';initParam el:' + el);
        console.log(TAG + ';initParam  width:' + this.width);
        console.log(TAG + ';initParam  height:' + this.height);
        console.log(TAG + ';initParam  center:' + this.center);
        console.log(TAG + ';initParam  radius:' + this.radius);
        console.log(TAG + ';initParam  ctx:' + this.ctx);
        console.log(TAG + ';initParam  outLineWidth:' + this.outLineWidth);

    }

    // 绘图
    draw(value) {
        console.log(TAG + ';draw  value:' + value);
        if (value == undefined) {
            value = this.value;
        }
        this.ctx.clearRect(0, 0, this.width, this.width);
        this.ctx.save();

        let startDeg = this.counterclockwise ? Math.PI * (2 - this.startAngle) : Math.PI * this.startAngle
        let endDeg = this.counterclockwise ? Math.PI * (2 - this.endAngle) : Math.PI * this.endAngle
      //  console.log(TAG + ';draw  startDeg:' + startDeg);
      //  console.log(TAG + ';draw  endDeg:' + endDeg);


        // 绘制内层圆弧
        //        this.ctx.beginPath();
        //        this.ctx.lineWidth = 1;
        //        this.ctx.arc(this.center, this.center, this.radius - 20, startDeg, endDeg, this.counterclockwise); // 绘制内层圆弧
        //        this.ctx.strokeStyle = this.innerColor;
        //        this.ctx.stroke();


        // 绘制外侧圆弧
        this.ctx.beginPath();
        this.ctx.arc(this.center, this.center, this.radius, startDeg,
            endDeg, this.counterclockwise);
        this.ctx.strokeStyle = this.outColor;
        this.ctx.lineCap = "round";
        this.ctx.lineWidth = this.outLineWidth;
        this.ctx.stroke();

        let Deg = this.valToDeg(value)

        if (value > 66) {

            // 绘制可变圆弧
            this.drawThree(startDeg, value);
        }

        if (value > 33) {
            // 绘制可变圆弧
            this.drawTwo(startDeg, value);
        }

        this.drawOne(startDeg, value);

        // 绘制滑块
        this.P = this.DegToXY(Deg)
      //  console.log(TAG + ' ;this.P.x:' + this.P.x);
      //  console.log(TAG + ' ;this.P.y:' + this.P.y);

        if (value >= 100) {
            this.ctx.beginPath();
            this.ctx.moveTo(this.center, this.center);
            this.ctx.arc(this.P.x, this.P.y, this.outLineWidth / 2, 0, Math.PI * 2);
            // this.ctx.arc(this.P.x, this.P.y, this.outLineWidth / 2 - this.slider, 0, Math.PI * 2, false); // 绘制滑块内侧
            this.ctx.fillStyle = "#2B5BF9";
            this.ctx.fill();
        }

        this.ctx.beginPath();
        this.ctx.moveTo(this.center, this.center);
        this.ctx.arc(this.P.x, this.P.y, this.slider, 0, Math.PI * 2, false); // 绘制滑块
        this.ctx.fillStyle = this.sliderColor;
        this.ctx.fill();

        // 文字
        if (this.textShow) {
            this.ctx.font = '60px HarmonyHeiTi, HarmonyHeiTi-Medium';
            this.ctx.fillStyle = "#000000";
            this.ctx.textAlign = "center"
            this.ctx.textBaseline = "middle";
            this.ctx.fillText(this.value + "", this.center, this.center);
        }

    }

    // 0 - =33
    drawOne(startDeg, value) {
        if (value > 33) {
            value = 33;
        }
        let Deg = this.valToDeg(value)
        // 绘制可变圆弧
        const grad = this.ctx.createLinearGradient(0, 0, 0, this.width * 2 / 3);
        // Add three color stops
        grad.addColorStop(0.0, '#86c1ff');
        grad.addColorStop(1.0, '#3460F8');

        this.ctx.beginPath();
        this.ctx.arc(this.center, this.center, this.radius, startDeg, Deg, this.counterclockwise); // 可变圆弧
        this.ctx.strokeStyle = grad;
        this.ctx.lineCap = "round";
        this.ctx.lineWidth = this.outLineWidth;
        this.ctx.stroke();
    }

    // 33 - =66
    drawTwo(startDeg, value) {
        if (value > 66) {
            value = 66;
        }
        let deg = this.valToDeg(value)
        // 绘制可变圆弧
        this.ctx.beginPath();
        this.ctx.arc(this.center, this.center, this.radius, startDeg, deg, this.counterclockwise); // 可变圆弧
        this.ctx.strokeStyle = "#3460F8";
        this.ctx.lineCap = "round";
        this.ctx.lineWidth = this.outLineWidth;
        this.ctx.stroke();
    }

    // 66 - =100
    drawThree(startDeg, value) {
        const grad = this.ctx.createLinearGradient(0, this.width * 2 / 3, 0, 0);
        // Add three color stops
        grad.addColorStop(0.0, '#3460F8');
        grad.addColorStop(1.0, '#2B5BF9');

        let deg = this.valToDeg(value)
        // 绘制可变圆弧
        this.ctx.beginPath();
        this.ctx.arc(this.center, this.center, this.radius, startDeg, deg, this.counterclockwise); // 可变圆弧
        this.ctx.strokeStyle = grad;
        this.ctx.lineCap = "round";
        this.ctx.lineWidth = this.outLineWidth;
        this.ctx.stroke();
    }

    //将值转化为弧度
    valToDeg(v) {
      //  console.log(TAG + ' ;valToDeg v:' + v);
        let range = this.endAngle - this.startAngle;
       // console.log(TAG + ' ;valToDeg range:' + range);
        let val = range / 100 * v;
      //  console.log(TAG + ' ;valToDeg val:' + val);
        if (this.counterclockwise && (val != 0)) val = 2 - val;
        let startDeg = this.counterclockwise ? (2 - this.startAngle) : this.startAngle;
        return (startDeg + val) * Math.PI;
    }
    // 弧度转化为对应坐标值
    DegToXY(deg) {
      //  console.log(TAG + '; DegToXY deg:' + deg);
        let d = 2 * Math.PI - deg;
      //  console.log(TAG + '; DegToXY d:' + d);
        return this.respotchangeXY({
            x: this.radius * Math.cos(d),
            y: this.radius * Math.sin(d)
        })
    }

    //canvas坐标转化为中心坐标
    spotchangeXY(point) {
        const spotchangeX = (i) => {
            return i - this.center
        }
        const spotchangeY = (i) => {
            return this.center - i
        }
        return {
            x: spotchangeX(point.x),
            y: spotchangeY(point.y)
        }
    }

    //中心坐标转化为canvas坐标
    respotchangeXY(point) {
        const spotchangeX = (i) => {
            return i + this.center
        }
        const spotchangeY = (i) => {
            return this.center - i
        }
        return {
            x: spotchangeX(point.x),
            y: spotchangeY(point.y)
        }
    }

    setLinearGradient() {
        const grad = this.ctx.createLinearGradient(0, 0, 0, this.width * 2 / 3);
        console.log(TAG + 'setLinearGradient  grad:' + grad);
        console.log(TAG + 'setLinearGradient  this.width:' + this.width);

        // Add three color stops
        // grad.addColorStop(0.0, 'red');
        grad.addColorStop(0.0, '#86c1ff');
        // grad.addColorStop(0.5, 'white');
        // grad.addColorStop(1.0, '#2B5BF9');
        grad.addColorStop(1.0, '#3460F8');
        // color = ["#86c1ff", "#2B5BF9"],
        //        this.color.forEach((e, i) => {
        //            console.log(TAG + 'setLinearGradient  e:' + e);
        //            console.log(TAG + 'setLinearGradient  i:' + i);
        //            if (i == 0) {
        //                grad.addColorStop(0, e)
        //            } else if (i == this.color.length - 1) {
        //                grad.addColorStop(1, e)
        //            } else {
        //                grad.addColorStop(1 / this.color.length * (i + 1), e);
        //            }
        //        });
        return grad;
    }

    OnMouseMove(evt) {
       // console.log(TAG + '; OnMouseMove start...');
        if (!this.isDown) return;
      //  console.log(TAG + '; OnMouseMove start1...');
        let evpoint = {};
        evpoint.x = this.getx(evt);
        evpoint.y = this.gety(evt);
        let point = this.spotchangeXY(evpoint);
        let deg = this.XYToDeg(point.x, point.y);
     //   console.log(TAG + '; OnMouseMove deg XYToDeg ...' + deg);
        deg = this.counterclockwise ? deg : Math.PI * 2 - deg;
      //  console.log(TAG + '; OnMouseMove deg...' + deg);

        let val = (deg / Math.PI - this.startAngle) / (this.endAngle - this.startAngle) * 100
      //  console.log(TAG + '; OnMouseMove val:' + val);
        if (val < 0) val = 100 + val;
        if (val <= 0) val = 0;
        if (val > 100) {
            if (this.value > 75) {
                val = 100;
            } else {
                val = val - 100;
            }
        }
        if (val > 75) {
            if (this.value < 25) {
                val = 0;
            }
        }

    //    console.log(TAG + '; OnMouseMove val2:' + val);
        if (Math.abs(val - this.value) > 10) return;
     //   console.log(TAG + '; OnMouseMove val:' + val);
        this.animate = requestAnimationFrame(this.draw.bind(this, val));
        if (this.value != Math.round(val)) {
            this.value = Math.round(val);
            this.change(this.value)
        }
     //   console.log(TAG + '; OnMouseMove end...');
    }

    OnMouseDown(evt) {
     //   console.log(TAG + ' ;OnMouseDown start...');
        let range = 10;
        let X = this.getx(evt);
        let Y = this.gety(evt);
        let P = this.P

//        console.log(TAG + ' ;OnMouseDown X:' + X);
//        console.log(TAG + ' ;OnMouseDown Y:' + Y);
//        console.log(TAG + ' ;OnMouseDown P.x :' + P.x);
//        console.log(TAG + ' ;OnMouseDown P.y :' + P.y);

        let minX = P.x - this.slider - range;
        let maxX = P.x + this.slider + range;
        let minY = P.y - this.slider - range;
        let maxY = P.y + this.slider + range;

//        console.log(TAG + ' ;OnMouseDown minX :' + minX);
//        console.log(TAG + ' ;OnMouseDown maxX :' + maxX);
//        console.log(TAG + ' ;OnMouseDown minY:' + minY);
//        console.log(TAG + ' ;OnMouseDown maxY :' + maxY);


        if (minX < X && X < maxX && minY < Y && Y < maxY) { //判断鼠标是否在滑块上
            this.isDown = true;
        } else {
            this.isDown = false;
        }
        console.log(TAG + 'OnMouseDown end...');
    }

    OnMouseUp() { //鼠标释放
        const _this = this
        cancelAnimationFrame(_this.animate);
        this.isDown = false
        console.log(TAG + 'OnMouseUp end...');
    }

    // 将坐标点转化为弧度
    XYToDeg(lx, ly) {
        console.log(TAG + '; XYToDeg ...lx:' + lx);
        console.log(TAG + '; XYToDeg ...ly:' + ly);
        let adeg = Math.atan(ly / lx)
        let deg;
        if (lx >= 0 && ly >= 0) {
            deg = adeg;
        }
        if (lx <= 0 && ly >= 0) {
            deg = adeg + Math.PI;
        }
        if (lx <= 0 && ly <= 0) {
            deg = adeg + Math.PI;
        }
        if (lx > 0 && ly < 0) {
            deg = adeg + Math.PI * 2;
        }
        return deg
    }

    //获取鼠标在canvas内坐标x
    getx(ev) {
        return ev.touches[0].localX;
    }

    //获取鼠标在canvas内坐标y
    gety(ev) {
        return ev.touches[0].localY;
    }

    //节流
    throttle(func) {
        let previous = 0;
        return function () {
            let now = Date.now();
            let context = this;
            let args = arguments;
            if (now - previous > 10) {
                func.apply(context, args);
                previous = now;
            }
        }
    }
}

//function main(){
//    const dom = document.getElementById("content")
//    const a = new DragAcr({
//        el: dom,
//        startDeg: 0.5,
//        endDeg: 2.5,
//        outColor: '#eee',
//        counterclockwise: true,
//        change: (v)=> {
//            console.log(`value:${v}`)
//        }
//    })
//}
