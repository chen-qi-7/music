export default {
    data: {
        on:"common/LAMP/ic_on.png", // 卡片图标
        off:"common/LAMP/ic_off.png", // 卡片图标
        statusText:"已关闭",
        cardicon:"common/LAMP/ic_off.png",
        isOn:false, // 是否已开启
    },
    control(){
        this.isOn = !this.isOn;
        if(this.isOn){
            this.statusText = "已关闭",
            this.cardicon=this.off;
        }else{
            this.statusText = "已开启",
            this.cardicon=this.on;
        }
    },


}
