// @ts-nocheck
// @ts-nocheck
export default {
    data: {
        title: 'World',
        ss:'1',
        add:"/common/add.png",
        reduce:"/common/reduce.png",
        device: {
            add: '/common/add.png',
            add1: '/common/add1.png',
            reduce: '/common/reduce.png',
            reduce1: '/common/reduce1.png',
        }
    },
    props: {
        // 最大值
        max: {
            type:Number,
            default: 5
        },
        // 最小值
        min: {
            type:Number,
            default: 0
        },
        // 当前值
        value: {
            type:Number,
            default: 0
        },
        itemText:{
            type:String,
            default: ''
        }
    },
    onInit() {
    },
    dxygenFlow(val) {
        if (val) {
            this.add = this.device.add1;
            this.value++;
        }
        else {
            this.reduce = this.device.reduce1;
            this.value--;
        }
        this.$emit('handleItemClick',this.value );
    },
    fingerLift() {
        this.add = this.device.add;
        this.reduce = this.device.reduce
    }
}
