export default {
    data: {
        title: 'World',
    },
    props:{
        message:{
            type:String,
            default:''
        }
    },
    cancelClick(){
        this.$element('hiLinkDialog').close();
    }

}
