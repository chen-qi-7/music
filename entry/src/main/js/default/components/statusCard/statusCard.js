

export default {
    props: {
        leftColor: {
        // 传入color.less定义好的颜色样式 如: c_E84026
            type: String,
            default: "#0A59F7",
        },
        leftTxt: {
            type: String,
            default: "待机中",
        },
        midColor: {
            type: String,
            default: "",
        },
        midTxt: {
            type: String,
            default: "",
        },
        remainingTimeTxt: {
            type: String,
            default: "剩余时间",
        },
        rightStatusIcon: {
            type: String,
            default: "/common/status/ic_standing.png",
        }




    },
    data(){
        return{

     }
        },

    onInit(){

    },




}