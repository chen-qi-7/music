/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import router from '@system.router';

export default {
    props: {
        // 设备标题
        title: {
            type:String,
            default: '',
        },
        // 是否显示右侧更多菜单
        showMenu: {
            type:Boolean,
            default: false
        },
        // 是否显示标题栏
        shownavbar: {
            type:Boolean,
            default: false
        },
        // 更多菜单列表
        menu: {
            type:Array,
            default: []
        }
    },

    onInit(){

    },
    // 返回
    backClick: function(){
        router.back();
    },
    // 点击更多图标事件
    menuClick: function(){
        this.$element('menu_opts').show();
    },
    // 点击更多菜单列表的具体项
    itemClick: function(e){
        this.$emit('menuItemClick', e);
    }

}