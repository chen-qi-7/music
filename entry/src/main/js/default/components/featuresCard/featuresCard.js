
/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import router from '@system.router';

export default {
    props: {

    },
    data: {
        isTopFiring: true,
        topFiring: "顶部加热",
        isRinse: false,
        rinse: '清洗',
        isFloodLight: false,
        floodLight: '照明灯',
        isSteamingRoastLights: false,
        steamingRoastLights: '蒸烤灯光',
        mode:{
            floodLight_off: '/common/mode/ic_floodLight_off.png',
            floodLight_on:'/common/mode/ic_floodLight_on.png',
            rinse_off:'/common/mode/ic_rinse_off.png',
            rinse_on:'/common/mode/ic_rinse_on.png',
            steamingRoastLights_off:'/common/mode/ic_steamingRoastLights_off.png',
            steamingRoastLights_on:'/common/mode/ic_steamingRoastLights_on.png',
            topFiring_off:'/common/mode/ic_topFiring_off.png',
            topFiring_on:'/common/mode/ic_topFiring_on.png',
        }
    },


    onInit() {
    },
    clickTopFiring () {
        this.isTopFiring = !this.isTopFiring
    },
    clickRinse () {
        this.isRinse = !this.isRinse
    },
    clickFloodLight () {
        this.isFloodLight = !this.isFloodLight
    },
    clickSteamingRoastLights () {
        this.isSteamingRoastLights = !this.isSteamingRoastLights
    }

}



