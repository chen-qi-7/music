export default {
    data: {
    },
    props:{
        // 是否需要小卡片 默认显示大卡片
        smallcard:{
            type:Boolean,
            default:false
        },
        // 是否已开启 默认是未开启
        isOn:{
            type:Boolean,
            default:false
        },
        // 卡片名称
        textName:{
            type:String,
            default:''
        },
        // 卡片灰色文案
        subtitle:{
            type:String,
            default:''
        },
        // 卡片图标
        icon:{
            type:String,
            default:''
        },
        // 卡片文案旁边图标
        subicon:{
            type:String,
            default:''
        },
        // 是否需要卡片文案旁边图标
        isSubicon:{
            type:Boolean,
            default:false
        },

    }

}
