import prompt from '@system.prompt';


export default {
    data: {
        title: 'World',
        back: "<",
        generic: "generic Access",
        deviceName: [],
        unknown: "Unknown Service",
        unknownCharacter: "Unknown Characteristic",
        uuid: "123456",
        properties: "READ",
        islinked: false,
        isShow: false,
        connectState: 0, // 0 连接中;1, 连接成功, 2连接失败,
        connectStateText: '连接中....'
    },


    onInit() {
        //        this.$element('mydialog').show()

    },
    connectBle: async function (index) {

        var THIS = this;
        var actionData = {};
        actionData.addr = THIS.deviceList[index].addr;
        var action = {};
        //        action这里面6项数据必填，
        action.bundleName = BUNDLE_NAME;
        action.abilityName = ABILTY_NAME;
        action.messageCode = CONNECT_BLE_CODE;
        action.data = actionData;
        action.abilityType = ABILITY_TYPE_INTERNAL;
        action.syncOption = ACTION_SYNC;
        //开始通信
        var ret = await FeatureAbility.callAbility(action);
        console.log("ret =" + ret)

    }
}
