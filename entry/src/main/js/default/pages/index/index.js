import router from '@system.router';

const BUNDLE_NAME = "com.cq.music";
const ABILTY_NAME = "com.cq.music.BlueToothAbility";
const GET_BLU_SATATUS_CODE = 1000;
const GO_BLU_OPEN_CODE = 1001;
const GO_BLU_CLOSE_CODE = 1002;
const SUBSCRIBE_BLUE_STATE = 1003;
const UNSUBSCRIBE_BLUE_STATE = 1004;
const BLE_SCAN_CODE = 1005;
const STOP_BLE_SCAN_CODE = 1006;
const CONNECT_BLE_CODE = 1008;
//const GET_BLE_SCAN_RESULT_CODE = 1007;
const ABILITY_TYPE_INTERNAL = 1;

// 0：同步 1：异步
const ACTION_SYNC = 0;

const STATE_OFF = 0;
const STATE_ON = 2;
const STATE_TURNING_OFF = 3;
const STATE_TURNING_ON = 1;

export default {
    data: {
        title: "",
        isFullScreen: true,
        bluOpen: false,
        icon: "<",
        showBlue: false,
        linkStatus: "未知",
        deviceList: [],
        link: "连接",
        loading: "加载中",
    },


    onInit() {
        // js 通过java 获取蓝牙状态;
        // this.getBlueStatus();
        this.subscribeAbilityEvent()

    },

    onDestroy() {
        this.unsubscribeAbilityEvent()
    },

    //    通信连接
    getBlueStatus: async function () {
        var actionData = {};
        var action = {};
        //        action这里面6项数据必填，
        action.bundleName = BUNDLE_NAME;
        action.abilityName = ABILTY_NAME;
        action.messageCode = GET_BLU_SATATUS_CODE;
        action.data = actionData;
        action.abilityType = ABILITY_TYPE_INTERNAL;
        action.syncOption = ACTION_SYNC;
        //开始通信
        var result = await FeatureAbility.callAbility(action);
        //        接受数据
        var ret = JSON.parse(result)
        //        ret.code  JAVA返回的消息
        console.info("cqlog blueStatus = " + ret.status)

        // public static final int STATE_OFF = 0;
        // public static final int STATE_ON = 2;
        // public static final int STATE_TURNING_OFF = 3;
        //  public static final int STATE_TURNING_ON = 1;
        this.getBlueState(ret.status);

    },
    enableBt: async function () {
        var actionData = {};
        var action = {};
        //        action这里面6项数据必填，
        action.bundleName = BUNDLE_NAME;
        action.abilityName = ABILTY_NAME;
        action.messageCode = GO_BLU_OPEN_CODE;
        action.data = actionData;
        action.abilityType = ABILITY_TYPE_INTERNAL;
        action.syncOption = ACTION_SYNC;
        //开始通信
        var result = await FeatureAbility.callAbility(action);
        //        接受数据
        var ret = JSON.parse(result)
        //        ret.code  JAVA返回的消息
        console.info("cqlog isOpen = " + ret.isOpen)
        this.bleScan();
    },
    disableBt: async function () {

        var action = {};
        //        action这里面6项数据必填，
        action.bundleName = BUNDLE_NAME;
        action.abilityName = ABILTY_NAME;
        action.messageCode = GO_BLU_CLOSE_CODE;
        action.abilityType = ABILITY_TYPE_INTERNAL;
        action.syncOption = ACTION_SYNC;
        //开始通信
        var result = await FeatureAbility.callAbility(action);
        //        接受数据
        var ret = JSON.parse(result)
        //        ret.code  JAVA返回的消息
        console.info("cqlog isClose = " + ret.isClose)
    },
    subscribeAbilityEvent: async function () {

        var action = {};
        //        action这里面6项数据必填，
        action.bundleName = BUNDLE_NAME;
        action.abilityName = ABILTY_NAME;
        action.messageCode = SUBSCRIBE_BLUE_STATE;
        action.abilityType = ABILITY_TYPE_INTERNAL;
        action.syncOption = ACTION_SYNC;
        var THIS = this;
        await FeatureAbility.subscribeAbilityEvent(action, function (callBackData) {
            var callBakcJson = JSON.parse(callBackData);
            THIS.getBlueState(callBakcJson.data.status);
            console.info("cqlog callBackData = " + JSON.stringify(callBakcJson));
        })
    },
    unsubscribeAbilityEvent() {
        var action = {};
        //        action这里面6项数据必填，
        action.bundleName = BUNDLE_NAME;
        action.abilityName = ABILTY_NAME;
        action.messageCode = UNSUBSCRIBE_BLUE_STATE;
        action.abilityType = ABILITY_TYPE_INTERNAL;
        action.syncOption = ACTION_SYNC;
        FeatureAbility.unsubscribeAbilityEvent(action)
    },
    //扫描蓝牙
    startBleScan: async function () {
        var action = {};
        //        action这里面6项数据必填，
        action.bundleName = BUNDLE_NAME;
        action.abilityName = ABILTY_NAME;
        action.messageCode = BLE_SCAN_CODE;
        action.abilityType = ABILITY_TYPE_INTERNAL;
        action.syncOption = ACTION_SYNC;
        var THIS = this;
        //开始通信
        await FeatureAbility.subscribeAbilityEvent(action, function (callBackData) {
            var callBakcJson = JSON.parse(callBackData);
            var data = {}
            data.name = callBakcJson.data.name;
            data.addr = callBakcJson.data.addr;
            THIS.deviceList.push(data);
            console.info("cqlog callBackData = " + JSON.stringify(callBakcJson));
        })
    },
    stopBleScan: async function () {
        var action = {};
        //        action这里面6项数据必填，
        action.bundleName = BUNDLE_NAME;
        action.abilityName = ABILTY_NAME;
        action.messageCode = STOP_BLE_SCAN_CODE;
        action.abilityType = ABILITY_TYPE_INTERNAL;
        action.syncOption = ACTION_SYNC;
        //开始通信
        var result = await FeatureAbility.callAbility(action);
        var ret = JSON.parse(result);
        console.info("result =" + ret);
    },
    connectBle: async function (index) {
        //
//        var THIS = this;
//        var actionData = {};
//        actionData.addr = THIS.deviceList[index].addr;
//        var action = {};
//        //        action这里面6项数据必填，
//        action.bundleName = BUNDLE_NAME;
//        action.abilityName = ABILTY_NAME;
//        action.messageCode = CONNECT_BLE_CODE;
//        action.data = actionData;
//        action.abilityType = ABILITY_TYPE_INTERNAL;
//        action.syncOption = ACTION_SYNC;
//        //开始通信
//        var ret = await FeatureAbility.callAbility(action);
//        console.log("ret =" + ret)
        router.push({
            uri: "pages/ble/ble",
            params: {
                deviceName: this.deviceList[index]
            }
        });

    },
    //封装
    getBlueState(state) {
        if (state == STATE_OFF) {
            this.linkStatus = "关闭"
        } else if (state == STATE_ON) {
            this.linkStatus = "打开"
        } else if (state == STATE_TURNING_OFF) {
            this.linkStatus = "关闭中..."
        } else if (state == STATE_TURNING_ON) {
            this.linkStatus = "打开中..."
        } else {
            this.linkStatus = "代码未知:" + state
        }
    },

    //    setBleScanDataListener: async function(){
    //            var action = {};
    //            //        action这里面6项数据必填，
    //            action.bundleName = BUNDLE_NAME;
    //            action.abilityName = ABILTY_NAME;
    //            action.messageCode = GET_BLE_SCAN_RESULT_CODE;
    //            action.abilityType = ABILITY_TYPE_INTERNAL;
    //            action.syncOption = ACTION_SYNC;
    //            var THIS = this;
    //            await FeatureAbility.subscribeAbilityEvent(action, function (callBackData) {
    //                var callBakcJson = JSON.parse(callBackData);
    //                var data = {}
    //                data.name = callBakcJson.data.name;
    //                THIS.deviceList.push(data);
    //                console.info("cqlog callBackData = " + JSON.stringify(callBakcJson));
    //            })
    //        }


}
